﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using GTA;
using GTA.Native;
using LCPDDevice.API;
using LCPDDevice.API.App;
using LCPDDevice.API.Collections;
using LCPDDevice.API.Managers;
using LCPDDevice.Helpers;

namespace Flashlight
{
	/// <summary>
	/// Spotlight & Flashlight Script based on original work from jitsuin666 @ lcpdfr.com
	/// This script is modified to use our API functionabilities.
	/// </summary>
	public class Flashlight : AppScript
	{
		private const float SPOTLIGHT_SPACING = 1f;
		private const float SPOTLIGHTS_COUNT = 100f;

		// General
		private bool spotLightOn;
		private bool flashLightOn;

		// Key Binding
		private Keys toggleKey;
		private Keys spotKeyUp, spotKeyDown, spotKeyLeft, spotKeyRight;
		private bool left, right, up, down;

		// Spotlight
		private float spotLight_Sensitivity;
		private float spotLight_Brightness;

		private Vehicle playerVeh, lastVeh;

		private float yawBank, pitchBank;
		private Vector3 spotDirection, spotPosition, vehOffset;
		private Light[] spotLights;

		// Flashlight
		private float flashLight_Brightness;

		private Light atHipLight;
		private Light[] aimLights;

		GTACollection configList;

		public Flashlight()
		{
			/**
			 * Default values
			 */
			spotLightOn		= false;
			flashLightOn	= false;

			/**
			 * Spotlight
			 */
			spotLights = LoadSpotLights();

			/**
			 * Flashlight
			 */
			atHipLight	= new Light(Color.WhiteSmoke, 3f, 15f);
			aimLights	= LoadAimLights();
		}

		public override void Init()
		{
			LoggerHelper.DebugMessage(this, "Flashlight script init...");

			/**
			 * Load settings from our ini file.
			 */
			LoadSettings();

			/**
			 * Key binding
			 */
			BindKey(toggleKey, OnToggleKey);
			KeyDown		+= Flashlight_KeyDown;
			KeyUp		+= Flashlight_KeyUp;

			/**
			 * Private collection with enabled cars and weapons spotlights
			 */
			configList = GTACollectionManager.GetInstance().LoadCollectionFromFile(this.GetCurrentDir() + "/config.xml");
		}

		private void Flashlight_KeyDown(object sender, GTA.KeyEventArgs e)
		{
			if( !spotLightOn )
				return;

			const bool v = true;

			if( e.Key == spotKeyUp )
				up = v;
			else if( e.Key == spotKeyDown )
				down = v;
			else if( e.Key == spotKeyLeft )
				left = v;
			else if( e.Key == spotKeyRight )
				right = v;
		}

		private void Flashlight_KeyUp(object sender, GTA.KeyEventArgs e)
		{
			if( !spotLightOn )
				return;

			const bool v = false;

			if( e.Key == spotKeyUp )
				up = v;
			else if( e.Key == spotKeyDown )
				down = v;
			else if( e.Key == spotKeyLeft )
				left = v;
			else if( e.Key == spotKeyRight )
				right = v;
		}

		private void LoadSettings()
		{
			// Key Binding
			toggleKey		= Settings.ReadKey("Keys",       "ToggleKey", Keys.H);
			spotKeyUp		= Settings.ReadKey("Keys",    "Spotlight_Up", Keys.NumPad8);
			spotKeyDown		= Settings.ReadKey("Keys",  "Spotlight_Down", Keys.NumPad2);
			spotKeyLeft		= Settings.ReadKey("Keys",  "Spotlight_Left", Keys.NumPad4);
			spotKeyRight	= Settings.ReadKey("Keys", "Spotlight_Right", Keys.NumPad6);

			// Spotlight Settings
			spotLight_Sensitivity	= Settings.ReadFloat("Spotlight", "Sensitivity", 1f);
			spotLight_Brightness	= Settings.ReadFloat("Spotlight",  "Brightness", 9f);
			spotLight_Brightness	= Math.Max(9f, spotLight_Brightness);

			// Flashlight Settings
			flashLight_Brightness	= Settings.ReadFloat("Flashlight", "Brightness", 4f);
			flashLight_Brightness	= Math.Max(4f, flashLight_Brightness);
		}

		private GameEpisode Episode()
		{
			return (GameEpisode)Function.Call<int>("GET_CURRENT_EPISODE");
		}

		private bool LeftTriggerPressed()
		{
			return Function.Call<bool>("IS_CONTROL_PRESSED", 0, 6);
		}

		private Light[] LoadAimLights()
		{
			return new Light[]
            {
                new Light(Color.WhiteSmoke, 0.25f, flashLight_Brightness + 15f),
                new Light(Color.WhiteSmoke,  0.5f, flashLight_Brightness + 14f),
                new Light(Color.WhiteSmoke,    1f, flashLight_Brightness + 13f),
                new Light(Color.WhiteSmoke, 1.25f, flashLight_Brightness + 12f),
                new Light(Color.WhiteSmoke,  1.5f, flashLight_Brightness + 11f),
                new Light(Color.WhiteSmoke, 1.75f, flashLight_Brightness + 10f),
                new Light(Color.WhiteSmoke,    2f, flashLight_Brightness + 9f),
                new Light(Color.WhiteSmoke, 2.25f, flashLight_Brightness + 8f),
                new Light(Color.WhiteSmoke,  2.5f, flashLight_Brightness + 7f),
                new Light(Color.WhiteSmoke, 2.75f, flashLight_Brightness + 6f),
                new Light(Color.WhiteSmoke,    3f, flashLight_Brightness + 5f),
                new Light(Color.WhiteSmoke, 3.25f, flashLight_Brightness + 4f),
                new Light(Color.WhiteSmoke,  3.5f, flashLight_Brightness + 3f),
                new Light(Color.WhiteSmoke, 3.75f, flashLight_Brightness + 2f),
                new Light(Color.WhiteSmoke,    4f, flashLight_Brightness + 1f),
                new Light(Color.WhiteSmoke, 4.25f, flashLight_Brightness),
                new Light(Color.WhiteSmoke,  4.5f, flashLight_Brightness - 1f),
                new Light(Color.WhiteSmoke, 4.75f, flashLight_Brightness - 2f),
                new Light(Color.WhiteSmoke,    5f, flashLight_Brightness - 3f),
                new Light(Color.WhiteSmoke, 5.25f, flashLight_Brightness - 4f)
            };
		}

		private Light[] LoadSpotLights()
		{
			Light[] lightArray = new Light[(int)SPOTLIGHTS_COUNT];
			for( float i = 0; i < SPOTLIGHTS_COUNT; i++ )
			{
				lightArray[(int)i] = new Light(Color.WhiteSmoke, 0.6f + 0.6f * i, spotLight_Brightness - 0.225f * i);
			}
			return lightArray;
		}

		public void OnToggleKey()
		{
			if( Game.LocalPlayer.Character.isInVehicle() )
				ToggleSpotlight();
			else
				ToggleFlashlight();
		}

		private void ToggleSpotlight()
		{
			Vehicle playerVehicle		= Game.LocalPlayer.Character.CurrentVehicle;

			GTACollectionCategory cat	= configList.GetCategoryById("CARS");
			if( cat == null )
				return;

			GTACollectionItem item		= cat.GetItemById(playerVehicle.Name);
			if( item != null )
			{
				spotLightOn = !spotLightOn;

				/**
				 * Notification
				 */
				string notifMsg			= "Spotlight switched " + (spotLightOn == true ? "on." : "off.");
				NotificationInfo notif	= new NotificationInfo(notifMsg, 2000);
				NotificationManager.GetInstance().AddNotification(this, notif, "SPOTLIGHT_TOGGLE");

				if( !Game.Exists(lastVeh) || lastVeh != playerVehicle )
					pitchBank = yawBank = 0;

				playerVeh = lastVeh = playerVehicle;

				if( spotLightOn )
				{
					EnableSpotLight(playerVehicle, item);
				}
				else
				{
					DisableSpotLight();
				}
			}
			else
			{
				LoggerHelper.DebugMessage(this, "OnToggleKey(): item is null");
			}
		}

		/**
		 * Spotlight methods
		 */

		private void EnableSpotLight(Vehicle playerVehicle, GTACollectionItem item)
		{
			vehOffset	  = StringHelper.ReadSafeVector(item.GetParamById("OFFSET", ""), Vector3.Zero);
			spotDirection = playerVehicle.Direction;
			spotPosition  = playerVeh.GetOffsetPosition(vehOffset);
			Tick		 += SpotLight_Tick;

			if( playerVeh.Name == "POLMAV" && pitchBank == 0 )
				pitchBank = -60f;
		}

		private void DisableSpotLight()
		{
			Tick -= SpotLight_Tick;
			foreach( Light light in spotLights )
				light.Disable();

			spotLightOn = false;
			playerVeh	= null;
			up = down = left = right = false;
		}

		private void SetBankValues()
		{
			if( Game.LocalPlayer.Character.isInVehicle(playerVeh) )
			{
				if( left )
					yawBank		+= spotLight_Sensitivity;
				else if( right )
					yawBank		-= spotLight_Sensitivity;
				if( up )
					pitchBank	+= spotLight_Sensitivity;
				else if( down )
					pitchBank	-= spotLight_Sensitivity;
			}

			if( playerVeh.Name == "POLMAV" )
			{
				pitchBank = pitchBank >= 0f ? 0f : (pitchBank <= -90f ? -90f : pitchBank);
				yawBank = Math.Abs(yawBank) == 360f ? 0f : yawBank;
			}
			else
			{
				pitchBank = pitchBank >= -10f ? (pitchBank <= 35f ? pitchBank : 35f) : -10f;
				yawBank = yawBank >= -90f ? (yawBank <= 90f ? yawBank : 90f) : -90f;
			}
		}

		private void SetDirection()
		{
			Vector3 playerVehDir	= playerVeh.Direction;
			Vector3 rotation		= Helper.DirectionToRotation(playerVehDir, 0f);
			rotation				= new Vector3(rotation.X + pitchBank, rotation.Y, rotation.Z + yawBank);
			spotDirection			= Helper.RotationToDirection(rotation);
		}

		private void UpdateLightPosition()
		{
			spotPosition = playerVeh.GetOffsetPosition(vehOffset);
			SetBankValues();
			SetDirection();
		}

		private bool PlayerVehLost()
		{
			return !Game.Exists(playerVeh) || !playerVeh.isAlive || (Game.LocalPlayer.Character.isInVehicle() && Game.LocalPlayer.Character.CurrentVehicle != playerVeh);
		}

		private void SpotLight_Tick(object sender, EventArgs e)
		{
			if( PlayerVehLost() )
			{
				LoggerHelper.DebugMessage(this, "Player Vehicle lost...");
				lastVeh			= null;
				spotLightOn		= false;
				DisableSpotLight();
				return;
			}

			UpdateLightPosition();

			for( int i = 0; i < spotLights.Length; i++ )
			{
				spotLights[i].Position = spotPosition + spotDirection * (float)(i * SPOTLIGHT_SPACING);
				spotLights[i].Enabled = true;
			}
		}

		/**
		 * Flashlight methods
		 */

		private void ToggleFlashlight()
		{
			flashLightOn = !flashLightOn;

			/**
			 * Notification
			 */
			string notifMsg			= "Flashlight switched " + (flashLightOn == true ? "on." : "off.");
			NotificationInfo notif	= new NotificationInfo(notifMsg, 2000);
			NotificationManager.GetInstance().AddNotification(this, notif, "FLASHLIGHT_TOGGLE");

			if( flashLightOn )
				EnableFlashLight();
			else
				DisableFlashLight();
		}

		private void EnableFlashLight()
		{
			Tick += FlashLight_Tick;
		}

		private void DisableFlashLight()
		{
			flashLightOn = false;
			Tick		-= FlashLight_Tick;

			atHipLight.Disable();
			foreach( Light light in aimLights )
				light.Disable();
		}

		private void FlashLight_Tick(object sender, EventArgs e)
		{
			Ped playerPed		= Game.LocalPlayer.Character;
			string weaponName	= playerPed.Weapons.CurrentType.ToString().ToUpperInvariant();

			if( playerPed.isInVehicle() || playerPed.Weapons.CurrentType == Weapon.None )
			{
				DisableFlashLight();
				return;
			}


			try
			{
				GTACollectionItem item = configList.GetCategoryById("WEAPONS").GetItemById(weaponName);
				if( item == null )
				{
					LoggerHelper.DebugMessage(this, "Flashlight disabled for " + weaponName);
					DisableFlashLight();
					return;
				}
			}
			catch ( Exception ex )
			{
				LoggerHelper.DebugMessage(this, "Flashlight exception: " + ex.ToString());
				DisableFlashLight();
				return;
			}

			atHipLight.Position = playerPed.GetBonePosition(Bone.RightHand);
			atHipLight.Enabled = true;

			if( LeftTriggerPressed() || Game.isGameKeyPressed(GameKey.Aim) )
			{
				atHipLight.Disable();
				for( int i = 0; i < aimLights.Length; i++ )
				{
					aimLights[i].Position	= Game.CurrentCamera.Position + Game.CurrentCamera.Direction * i;
					aimLights[i].Enabled	= true;
				}
			}
			else
			{
				foreach( Light light in aimLights )
				{
					light.Disable();
				}
			}
		}
	}
}
