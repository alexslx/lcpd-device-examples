﻿using System.Drawing;
using GTA;
using GTA.Forms;
using LCPDDevice.API;
using LCPDDevice.API.App;
using LCPDDevice.Helpers;

namespace Flashlight
{
	public class FlashlightUI : AppUI
	{
		/**
		 * Just saving the controls to use them directly.
		 * You do not need to unload them, the application base will do that.
		 * However, if you create any sub element like GTA.Font, you need to take care of that.
		 */
		private Button toggleButton;

		public FlashlightUI()
		{
			/**
			 * This constructor is called when the device is loaded and search for all installed applications.
			 * You should load all static resources here (such as images or files), but SHOULDN'T try to add any UI component yet.
			 */
		}

		/// <summary>
		/// This method is called once when the class is initialized and all settings are loaded.
		/// You should load all things that are dependent of settings or other resources.
		/// </summary>
		public override void Init()
		{
			
		}

		/// <summary>
		/// This method is called every time that the device want to show your application screen.
		/// Here is where you should load any UI component and add it to the control list.
		/// </summary>
		public override void Load()
		{
			/**
			 * The base method should always be called by your application.
			 */
			base.Load();

			this.SetFormColor(Color.DarkGray);

			/**
			 * This method loads the xml with your app layout and automatically puts it on the control list.
			 */
			this.AddControls(LayoutHelper.LoadLayout(this.GetCurrentDir() + "/layout.xml"));

			/**
			 * Here it is possible to get the current loaded controls by its names.
			 */
			toggleButton = (Button)GetControlByName("ToggleButton");

			toggleButton.Location	= new Point(0, 0);
			toggleButton.Size		= GetSize();
			toggleButton.Height		= 30;
			toggleButton.Click		+= toggleButton_Click;
		}

		private void toggleButton_Click(object sender, MouseEventArgs e)
		{
			
		}
	}
}
