﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("FlashlightUI")]
[assembly: AssemblyDescription("FlashlightUI application for LCPD Device")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("http://www.alexslx.com.br/")]
[assembly: AssemblyProduct("LCPD Device")]
[assembly: AssemblyCopyright("Copyright © 2014 Alexandre Leites")]
[assembly: AssemblyTrademark("Alexandre Leites <http://www.alexslx.com.br/>")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("89195737-e0bb-4d42-acf3-dae94132325f")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("0.0.1.0")]
[assembly: AssemblyFileVersion("0.0.1.0")]
