﻿using System;
using System.Drawing;
using GTA;
using GTA.Forms;
using LCPDDevice.API;
using LCPDDevice.API.App;
using LCPDDevice.API.Collections;
using LCPDDevice.API.Managers;
using LCPDDevice.Helpers;

namespace CarSpawner
{
	public class CarSpawner : AppUI
	{
		/**
		 * Just saving the controls to use them directly.
		 * You do not need to unload them, the application base will do that.
		 * However, if you create any sub element like GTA.Font, you need to take care of that.
		 */
		private Label appTitle;
		private Listbox catList;
		private Listbox carList;
		private Button spawnButton;

		/**
		 * Remember to cleanup it on Close() method.
		 */
		private GTA.Font titleFont;

		public CarSpawner()
		{
			/**
			 * This constructor is called when the device is loaded and search for all installed applications.
			 * You should load all static resources here (such as images or files), but SHOULDN'T try to add any UI component yet.
			 * Also, it is not recommended to try to load anything that requires all depedencies loaded yet (e.g: Settings or GetCurrentDir() method).
			 */
		}

		public override void Init()
		{
			/**
			 * This method is called once all dependencies are properly loaded. Here you can read your settings or GetCurrentDir() method.
			 * At this moment, you do not need to call the base method, but it is a good pratice.
			 */
			base.Init();
		}

		/// <summary>
		/// This method is called every time that the device want to show your application screen.
		/// Here is where you should load any UI component and add it to the control list.
		/// </summary>
		public override void Load()
		{
			/**
			 * The base method should always be called by your application.
			 */
			base.Load();

			this.SetFormColor(Color.DarkGray);

			/**
			 * This method loads the xml with your app layout and automatically puts it on the control list.
			 */
			this.AddControls(LayoutHelper.LoadLayout(this.GetCurrentDir() + "/layout.xml"));

			/**
			 * Here it is possible to get the current loaded controls by its names.
			 */
			appTitle	= (Label)GetControlByName("AppTitle");
			catList		= (Listbox)GetControlByName("CatList");
			carList		= (Listbox)GetControlByName("CarList");
			spawnButton = (Button)GetControlByName("SpawnButton");

			/**
			 * Here I create a font to use on the title.
			 * Currently, there is no way to create fonts on the XML.
			 * Also, you need to manage and correctly Dispose() it when the form is closed.
			 * If you don't do that, the device will crash after some time...
			 */
			titleFont	= new GTA.Font(20f, FontScaling.Pixel);

			/**
			 * The GetSize() function get the current screen size.
			 * I use it to simple extend my label along all the screen and after set the height I want.
			 */
			appTitle.Size			= GetSize();
			appTitle.Height			= 30;
			appTitle.Font			= titleFont;

			/**
			 * Here I use the location+height of last element (title) to set as start position of this element.
			 * Therefore, it will be placed right below the title.
			 */
			catList.Location		= new Point(0, appTitle.Location.Y + appTitle.Height);
			catList.Size			= GetSize();
			catList.Height			= ((GetSize().Height - 70) / 2);
			catList.SelectedIndexChanged += catList_SelectedIndexChanged;

			/**
			 * And here I still doing the same thing, but I set the height as half of the screen minus the other components size.
			 */
			carList.Location		= new Point(0, catList.Location.Y + catList.Height + 2);
			carList.Size			= GetSize();
			carList.Height			= ((GetSize().Height - 70) / 2);

			spawnButton.Location	= new Point(0, carList.Location.Y + carList.Height + 2);
			spawnButton.Size		= GetSize();
			spawnButton.Height		= 30;
			spawnButton.Click		+= spawnButton_Click;

			/**
			 * Here I get a pre-loaded collection (CARS) and add the categories on the first ListBox.
			 */
			GTACollectionManager colManager = GTACollectionManager.GetInstance();
			GTACollection carCollection		= colManager.GetCollection("CARS");
			if(carCollection != null)
			{
				foreach(GTACollectionCategory cat in carCollection.GetCategoryList())
					catList.Items.Add(cat, cat.GetCategoryName());
			}
		}

		private void catList_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			carList.Items.Clear();

			/**
			 * Here I check if the clicked element is valid, and then load the car list on the second ListBox.
			 */
			if(catList.SelectedValue != null)
			{
				GTACollectionCategory cat = (GTACollectionCategory)catList.SelectedValue;
				foreach(GTACollectionItem item in cat.GetItemList())
					carList.Items.Add(item, item.GetParamById("NAME", item.GetId()));
			}
		}

		private void spawnButton_Click(object sender, MouseEventArgs e)
		{
			if( carList.Items.Count == 0 )
				return;

			if( carList.SelectedValue == null )
				return;

			/**
			 * Finally, when the user click on the spawn button, I check if the selected car is valid.
			 * After that, I load the car using the normal GTA functions API.
			 */
			GTACollectionItem item = (GTACollectionItem)carList.SelectedValue;

			try
			{
				Vehicle veh = World.CreateVehicle(item.GetId(), Game.LocalPlayer.Character.Position.Around(5f));
				if( veh != null )
				{
					veh.PlaceOnGroundProperly();

					/**
					 * Create a simple notification to demonstrate the method.
					 */
					NotificationInfo notif = new NotificationInfo("Vehicle spawned.", 2000);
					NotificationManager.GetInstance().AddNotification(this, notif, "VEHICLE_SPAWNED");

					/**
					 * Here I close the form. This should be the last thing you call.
					 */
					DeviceManager.GetInstance().CloseDevice();
				}
				else
					LoggerHelper.DebugMessage(this, "Not possible to spawn the car: " + item.GetId() + " - " + item.GetParamById("NAME", item.GetId()));
			}
			catch( Exception ex )
			{
				LoggerHelper.DebugMessage(this, "Error spawning car: " + ex.ToString());
			}
		}

		/// <summary>
		/// This method is called when the form is about to close.
		/// Always call the base as it will cleanup every control for you.
		/// However, if you have created new GTA.Font elements, you should clean them here.
		/// </summary>
		public override void Close()
		{
			base.Close();

			/**
			 * Here I dispose of the GTA.Font used on the AppTitle component.
			 */
			if( titleFont != null )
			{
				titleFont.Dispose();
				titleFont = null;
			}
		}
	}
}
