﻿using System.Drawing;
using GTA;
using GTA.Forms;
using LCPDDevice.API.App;
using LCPDDevice.Helpers;

namespace HelloWorld
{
	public class HelloWorld : AppUI
	{
		public HelloWorld()
		{
			/**
			 * This constructor is called when the device is loaded and search for all installed applications.
			 * You should load all static resources here (such as images or files), but SHOULDN'T try to add any UI component yet.
			 */
		}

		/// <summary>
		/// This method is called every time that the device want to show your application screen.
		/// Here is where you should load any UI component and add it to the control list.
		/// </summary>
		public override void Load()
		{
			/**
			 * The base method should always be called by your application.
			 */
			base.Load();

			this.SetFormColor(Color.DarkGray);

			this.AddControls(LayoutHelper.LoadLayout(this.GetCurrentDir() + "/layout.xml"));

			Label title		= (Label)this.GetControlByName("AppTitle");
			title.Font		= new GTA.Font(26f, FontScaling.Pixel);

			int y			= (title.Location.Y + title.Size.Height) + 50;

			Textbox text	= (Textbox)this.GetControlByName("AppText");
			text.Text		= Settings.ReadString("Configuration", "Message", "Hello My World!");
			text.Size		= new Size(this.GetSize().Width - 20, this.GetSize().Height - y - 20);
			text.Location	= new Point(10, y);
		}
	}
}
